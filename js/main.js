
var edad = document.getElementById('edad');
var altura = document.getElementById('altura');
var peso = document.getElementById('peso');
var imc = document.getElementById('imc');
var prom = 0.0;



function generar() {

  // Edad
  const edadRandom = [];

  edadRandom.push(Math.floor(Math.random() * (99 - 18) + 18));

  edad = edadRandom;
  document.getElementById('edad').value = edad;

  // Altura
  const alturaRandom = [];

  alturaRandom.push(Math.random().toFixed(2) * (1.5 - 2.5) + 2.5);

  altura = alturaRandom;
  document.getElementById('altura').value = altura;

  // Peso
  const pesoRandom = [];

  pesoRandom.push(Math.random().toFixed(2) * (19.9 - 129.9) + 129.9);

  peso = pesoRandom;
  document.getElementById('peso').value = peso;

}

function calcular() {


  var nivel = document.getElementById('nivel');

  document.getElementById('imc').value = (peso / (altura * altura)).toFixed(2);

  imc = document.getElementById('imc').value;

  if (imc < 18.5) {

    document.getElementById('nivel').value = "Bajo peso";

  } else if (imc >= 18.5 && imc <= 24.9) {

    document.getElementById('nivel').value = "Peso saludable";

  } else if (imc >= 25.0 && imc <= 29.9) {

    document.getElementById('nivel').value = "Sobrepeso";

  } else if (imc > 30.0) {

    document.getElementById('nivel').value = "Obesidad";

  }

}

var num = 0.0;

function registrar() {
  
  if (num <= 19) {

    num = num + 1;
    prom = Number(prom) + Number(imc);
    document.getElementById('prom').value = (prom / num).toFixed(2);

    const lista = document.querySelector("#lista1");
    const lista2 = document.createElement("lista2");

    lista2.textContent = " - Num: " + num + "| Edad: " + edad + " | Altura: " + altura + " | Peso: " + peso + " | IMC: " + document.getElementById('imc').value + " | Nivel: " + document.getElementById('nivel').value
    lista.appendChild(lista2);

  } else {

    alert("Ya ha alcanzado las 20 personas!");

  }
}

function Borrar() {
  const lista = document.querySelector("#lista1");
  lista.remove();
  alert("Se han borrado los registros!");
  location.reload();
}

